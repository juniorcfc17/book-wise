import { Star } from "@phosphor-icons/react";
import Image from "next/image";

interface AssessmentsProps {
  imageBook: string
  photoUrl: string
  name: string
  bookTitle: string
  bookAuthor: string
  bookDescription: string
  stars?: number
}

export default function Assessments({ imageBook, bookAuthor, bookDescription, bookTitle, name, photoUrl, stars }: AssessmentsProps) {
  const defaultStars = 5;

  return (
    <div className="flex flex-col gap-8 p-6 bg-gray-700 rounded-lg">
      <div className="flex justify-between">
        <div className="flex gap-4">
          <div className="w-11 h-11 p-[1px] rounded-full bg-gradient-to-b from-green-400 to-purple-300">
            <Image
              src={photoUrl}
              className="rounded-full"
              alt="Foto Perfil"
              width={44}
              height={44}
              priority
            />
          </div>
          <span className="flex flex-col text-base text-gray-100 font-normal">
            {name}
            <span className="mt-1 text-sm text-gray-400 font-normal">Hoje</span>
          </span>
        </div>

        <div className="flex gap-1">
          {[...Array(defaultStars)].map((_, index) => {
            const isFilled = index < stars!;

            return (
              <div key={index}>
                {isFilled ? (
                  <Star width={16} height={16} color="#8381D9" weight="fill" />
                ) : (
                  <Star width={16} height={16} color="#8381D9" />
                )}
              </div>
            );
          })}
        </div>
      </div>

      <div className="h-[152px] flex gap-5">
        <Image src={imageBook} width={108} alt="Livro" />

        <div className="w-full h-[152px] flex flex-col gap-5">
          <span className="flex flex-col text-base text-gray-100 font-bold">
            {bookTitle}
            <span className="mt-1 text-sm text-gray-400 font-normal">{bookAuthor}</span>
          </span>

          <span className="h-[88px] text-base font-normal text-gray-300 leading-base text-ellipsis overflow-hidden">
            {bookDescription}
          </span>
        </div>
      </div>
    </div>

  )
}