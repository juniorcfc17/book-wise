import type { Metadata } from 'next'
import NextAuthSessionProvider from '../providers/sessionProvider'

export const metadata: Metadata = {
  title: 'Login | BookWise'
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <body className='font-default bg-gray-800'>
      <NextAuthSessionProvider>
        {children}
      </NextAuthSessionProvider>
    </body>
  )
}
