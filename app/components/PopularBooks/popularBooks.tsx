import { Star } from "@phosphor-icons/react";
import Image from "next/image";

interface PopularBooksProps {
  imageBook: string
  bookTitle: string
  bookAuthor: string
  stars?: number
}

export default function PopularBooks({ imageBook, bookTitle, bookAuthor, stars }: PopularBooksProps) {
  const defaultStars = 5;

  return (
    <div className="flex gap-5 px-5 py-4 rounded-lg mt-3 bg-gray-700">
      <Image src={imageBook} alt="" priority />

      <div className="flex flex-col justify-between">
        <span className="flex flex-col text-base text-gray-100 font-bold">
          {bookTitle}
          <span className="text-sm text-gray-400 font-normal">{bookAuthor}</span>
        </span>
        <div className="flex gap-1">
          {[...Array(defaultStars)].map((_, index) => {
            const isFilled = index < stars!;

            return (
              <div key={index}>
                {isFilled ? (
                  <Star width={16} height={16} color="#8381D9" weight="fill" />
                ) : (
                  <Star width={16} height={16} color="#8381D9" />
                )}
              </div>
            );
          })}
        </div>

      </div>

    </div>
  )
}