'use client'
import Image from 'next/image'
import sidebarImage from "../../assets/sidebar.svg"
import logoImage from "../../assets/logo.svg"
import Link from 'next/link'
import { Binoculars, ChartLineUp, SignIn, User } from '@phosphor-icons/react'
import { usePathname } from 'next/navigation'

export default function Sidebar() {
  const pathName = usePathname();

  return (
    <div className="w-[232px] h-full flex justify-center relative">
      <Image src={sidebarImage} alt="Imagem background" className="w-full h-full object-cover rounded-[10px]" priority={true} />
      <div className="absolute">
        <Image src={logoImage} alt="Logo" width={128} height={32} className='mt-10' priority={true} />

        <nav className='h-full flex flex-col gap-4 mt-16'>
          <ul>
            <Link href='/'>
              <li className={`flex gap-4 items-center pb-4 text-base ${pathName === '/' ? 'text-gray-100 font-bold' : 'text-gray-400 font-normal'} `}>
                <div className={`w-1 h-6 ${pathName === '/' ? 'bg-gradient-to-t from-[#7FD1CC] to-[#9694F5] rounded-lg' : 'bg-transparent'}`} />
                <ChartLineUp width={24} height={24} />
                Início
              </li>
            </Link>
            <Link href='/explore'>
              <li className={`flex gap-4 items-center py-4 text-base ${pathName === '/explore' ? 'text-gray-100 font-bold' : 'text-gray-400 font-normal'}`}>
                <div className={`w-1 h-6 ${pathName === '/explore' ? 'bg-gradient-to-t from-[#7FD1CC] to-[#9694F5] rounded-lg' : 'bg-transparent'}`} />
                <Binoculars width={24} height={24} />
                Explorar
              </li>
            </Link>
            <Link href='/profile'>
              <li className={`flex gap-4 items-center py-4 text-base ${pathName === '/profile' ? 'text-gray-100 font-bold' : 'text-gray-400 font-normal'}`}>
                <div className={`w-1 h-6 ${pathName === '/profile' ? 'bg-gradient-to-t from-[#7FD1CC] to-[#9694F5] rounded-lg' : 'bg-transparent'}`} />
                <User width={24} height={24} />
                Perfil
              </li>
            </Link>


            <Link href='/login'>
              <li className='flex gap-4 items-center pr-4 py-2 mb-8 text-base text-gray-200 font-bold fixed bottom-0'>
                Fazer Login
                <SignIn width={24} height={24} color='#50B2C0' />
              </li>
            </Link>
          </ul>



        </nav>
      </div>
    </div>
  )
}