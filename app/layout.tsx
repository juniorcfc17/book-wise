import type { Metadata } from 'next'
import './globals.css'
import Sidebar from './components/Sidebar/sidebar'
import NextAuthSessionProvider from './providers/sessionProvider'

export const metadata: Metadata = {
  title: 'BookWise'
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="pt-br">
      <body className='w-screen h-screen p-5 flex font-default bg-gray-800'>
        <NextAuthSessionProvider>
          <Sidebar />
          {children}
        </NextAuthSessionProvider>
      </body>
    </html>
  )
}
