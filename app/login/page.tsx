'use client'
import { signIn, useSession } from "next-auth/react";
import Image from "next/image";
import backgroundImage from "../assets/backgroundImage.svg"
import logoImage from "../assets/logo.svg"
import googleLogo from "../assets/logos_google-icon.svg"
import gitHubLogo from "../assets/akar-icons_github-fill.svg"
import visitorLogo from "../assets/RocketLaunch.svg"
import Link from "next/link";
import { useRouter } from "next/navigation";

export default function Login() {
  const { status } = useSession()
  const { push } = useRouter()

  if (status == 'authenticated') {
    push('/')
  }

  const auth = () => {
    signIn('google')
  }
  return (
    <div className="w-screen h-screen flex px-5">
      <div className="relative flex items-center justify-center">
        <Image src={backgroundImage} alt="Imagem background" className="rounded-[10px]" priority />
        <div className="absolute">
          <Image src={logoImage} alt="Logo" priority />
        </div>
      </div>

      <div className="flex flex-1 flex-col justify-center items-center">

        <div className="w-[372px]">
          <h2 className="text-2xl text-gray-100 font-bold font-default leading-9">Boas Vindas!</h2>
          <span className="text-base text-gray-200 font-normal font-default leading-6">Faça seu login ou acesse como visitante.</span>
        </div>

        <div className="w-[372px] mt-10 flex flex-col gap-4">
          <span className="flex gap-5 items-center px-6 py-5 bg-gray-600 rounded-lg hover:bg-gray-600/70">
            <Image src={googleLogo} width={32} alt="Logo Google" priority />
            <button
              onClick={auth}
              className="text-lg text-gray-200 font-bold font-default leading-9">
              Entrar com Google
            </button>
          </span>
          <span className="flex gap-5 items-center px-6 py-5 bg-gray-600 rounded-lg cursor-pointer hover:bg-gray-600/70">
            <Image src={gitHubLogo} width={32} alt="Logo GitHub" priority />
            <span className="text-lg text-gray-200 font-bold font-default leading-9">Entrar com GitHub</span>
          </span>
          <Link href='/'>
            <span className="flex gap-5 items-center px-6 py-5 bg-gray-600 rounded-lg cursor-pointer hover:bg-gray-600/70">
              <Image src={visitorLogo} width={32} alt="Logo Visitante" priority />
              <span className="text-lg text-gray-200 font-bold font-default leading-9">Acessar como visitante</span>
            </span>
          </Link>
        </div>

      </div>

    </div>
  )
}