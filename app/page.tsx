'use client'
import { CaretRight, ChartLineUp } from "@phosphor-icons/react";
import Link from "next/link";
import Assessments from "./components/Assessments/assessments";
import PopularBooks from "./components/PopularBooks/popularBooks";
import book from './assets/book.svg'
import book1 from './assets/book1.svg'
import bookP from './assets/bookP.svg'
import bookP1 from './assets/bookP1.svg'
import bookP2 from './assets/bookP2.svg'
import bookP3 from './assets/bookP3.svg'


export default function Home() {
  return (
    <div className="w-full h-full flex gap-[63px] px-[93px] justify-between pt-12">
      <div className="w-full flex flex-col gap-10">
        <div className="flex items-center gap-3 font-bold text-2xl text-gray-100">
          <ChartLineUp width={32} height={32} color="#50B2C0" />
          Início
        </div>

        {/* AVALIAÇÕES RECENTES */}
        <div className="flex flex-col overflow-y-hidden">
          <span className="font-normal text-sm text-gray-100">Avaliações mais recentes</span>

          <div className="w-full flex flex-col gap-3 mt-4 overflow-y-scroll">
            <Assessments
              imageBook={book}
              bookAuthor="J.R.R. Tolkien"
              bookDescription="Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis tempora quaerat magnam doloribus, placeat laborum eligendi ipsa nesciunt laboriosam quibusdam velit voluptatem dolorum molestias cum nihil, nemo, dolor culpa molestiae."
              bookTitle="O Hobbit"
              name="Jaxson Dias"
              photoUrl="https://github.com/adilsonjrs.png"
              stars={3}
            />
            <Assessments
              imageBook={book1}
              bookAuthor="Douglas Adams"
              bookDescription="Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis tempora quaerat magnam doloribus, placeat laborum eligendi ipsa nesciunt laboriosam quibusdam velit voluptatem dolorum molestias cum nihil, nemo, dolor culpa molestiae."
              bookTitle="O guia do mochileiro das galáxias"
              name="Brandon Botosh"
              photoUrl="https://github.com/diego3g.png"
              stars={5}
            />
            <Assessments
              imageBook={book1}
              bookAuthor="Douglas Adams"
              bookDescription="Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis tempora quaerat magnam doloribus, placeat laborum eligendi ipsa nesciunt laboriosam quibusdam velit voluptatem dolorum molestias cum nihil, nemo, dolor culpa molestiae."
              bookTitle="O guia do mochileiro das galáxias"
              name="Brandon Botosh"
              photoUrl="https://github.com/diego3g.png"
              stars={5}
            />
            <Assessments
              imageBook={book1}
              bookAuthor="Douglas Adams"
              bookDescription="Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis tempora quaerat magnam doloribus, placeat laborum eligendi ipsa nesciunt laboriosam quibusdam velit voluptatem dolorum molestias cum nihil, nemo, dolor culpa molestiae."
              bookTitle="O guia do mochileiro das galáxias"
              name="Brandon Botosh"
              photoUrl="https://github.com/diego3g.png"
              stars={5}
            />
            <Assessments
              imageBook={book1}
              bookAuthor="Douglas Adams"
              bookDescription="Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis tempora quaerat magnam doloribus, placeat laborum eligendi ipsa nesciunt laboriosam quibusdam velit voluptatem dolorum molestias cum nihil, nemo, dolor culpa molestiae."
              bookTitle="O guia do mochileiro das galáxias"
              name="Brandon Botosh"
              photoUrl="https://github.com/diego3g.png"
              stars={5}
            />
          </div>
        </div>
      </div>

      {/* LIVROS POPULARES */}
      <div className="w-[600px] pt-[70px]">
        <div className="w-full flex justify-between px-1 py-2">
          <span className="font-normal text-gray-100 text-sm">Livros populares</span>
          <Link href='/'>
            <span className="flex gap-2 items-center font-bold text-purple-100 text-sm hover:text-purple-100/80">
              Ver todos
              <CaretRight size={16} />
            </span>
          </Link>
        </div>

        <div className="w-full">
          <PopularBooks imageBook={bookP} bookTitle="A revolução dos bichos" bookAuthor="George Orwell" stars={4} />
          <PopularBooks imageBook={bookP1} bookTitle="14 Hábitos de Desenvolvedores Altament Produtivos" bookAuthor="Zeno Rocha" stars={4} />
          <PopularBooks imageBook={bookP2} bookTitle="O Fim da Eternidade" bookAuthor="Isaac Asimov" stars={4} />
          <PopularBooks imageBook={bookP3} bookTitle="Entendendo Algoritmos" bookAuthor="Aditya Bhargava" stars={4} />
        </div>
      </div>
    </div>
  )
}
